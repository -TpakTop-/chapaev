namespace Battle{
    public class PlayerManager{
        public Player Player { get; private set; }
        public Player Enemy { get; private set; }

        public PlayerManager(Player player, Player enemy) {
            Player = player;
            Enemy = enemy;
        }
    }
}