using UnityEngine;

namespace Battle{
    [RequireComponent(typeof(LineRenderer))]
    public class DirectionVector : MonoBehaviour{
        [SerializeField] private float _maxSize = 4f;
        private LineRenderer _lineRenderer;

        private void Awake() {
            _lineRenderer = GetComponent<LineRenderer>();
            _lineRenderer.positionCount = 2;
            _lineRenderer.SetPositions(new []{Vector3.zero, Vector3.zero});
        }

        public void Show(Vector3 startPosition, Vector3 finishPosition) {
            finishPosition = new Vector3(finishPosition.x, startPosition.y, finishPosition.z);
            Vector3 vector = finishPosition - startPosition;
            if (vector.magnitude > _maxSize) {
                finishPosition = startPosition + vector.normalized * _maxSize;
            }
            _lineRenderer.SetPosition(0, startPosition);
            _lineRenderer.SetPosition(1, finishPosition);
        }

        public void Hidden() {
            _lineRenderer.SetPosition(0, Vector3.zero);
            _lineRenderer.SetPosition(1, Vector3.zero);
        }
    }
}