using System.Collections.Generic;
using UnityEngine;

namespace Battle{
    public class LivingSpace : MonoBehaviour{
        public List<Chequer> ChipsOutOfChequers { get; private set; }

        private void Awake() {
            ChipsOutOfChequers = new List<Chequer>();
        }

        private void OnTriggerExit(Collider other) {            
            Chequer chequer = other.GetComponent<Chequer>();
            if (chequer) {
                chequer.IsLive = false;
                ChipsOutOfChequers.Add(chequer);
            }            
        }

        public void ClearChipsOutOfChequers() {
            ChipsOutOfChequers.Clear();
        }
    }
}