using System.Collections;
using System.Collections.Generic;
using Battle;
using Battle.AI;
using UnityEditor;
using UnityEngine;

namespace Battle{
    public class BattleManager : MonoBehaviour, IDragHandler<Chequer, Chequer>{
        public const string OPPONENT_TURNS = "OPPONENT_TURNS";
        public const string ANOTHER_ATTEMPT = "ANOTHER_ATTEMPT";
        public const string WIN_PLAYER_ROUND = "WIN_PLAYER_ROUND";
        public const string WIN_PLAYER_GAME = "WIN_PLAYER_GAME";
        [SerializeField] private DirectionVector _directionVector;
        [SerializeField] private Board _board;
        [SerializeField] private LivingSpace _livingSpace;
        private IAI _AIController;
        public PlayerManager PlayerManager { get; private set; }
        private Chequer _selectChequer;
        public bool IsCurrentWhiteSide { get; private set; }
        private bool _isWaitStopped;

        public void InitGame(bool playerIsWhite, IAI AIController) {
            PlayerManager = new PlayerManager(
                new Player("Player", playerIsWhite, playerIsWhite ? _board.WhiteChequer : _board.BlackChequer),
                new Player("AI", !playerIsWhite, !playerIsWhite ? _board.WhiteChequer : _board.BlackChequer));
            _AIController = AIController;
            _AIController.Init(PlayerManager.Enemy, PlayerManager.Player, this);
            _AIController.Stopped();
            _directionVector.Hidden();
            IsCurrentWhiteSide = true;
        }

        public void StartGame() {
            _board.SetChequers(PlayerManager.Player);
            _board.SetChequers(PlayerManager.Enemy);            
            _isWaitStopped = false;
            MakeStep();
        }

        public void OnBeginDrag(Chequer selectComponent, Vector3 position) {
            if (!_isWaitStopped && selectComponent && selectComponent.IsLive &&
                IsCurrentWhiteSide.Equals(selectComponent.IsWhiteColor)) {
                _selectChequer = selectComponent;
            }
        }

        public void OnDrag(Chequer selectComponent, Vector3 position) {
            if (_selectChequer == selectComponent) {
                _directionVector.Show(_selectChequer.transform.position, position);
            }
        }

        public void OnEndDrag(Chequer selectComponent, Vector3 position, Chequer targetComponent) {
            if (!_selectChequer) return;
            _directionVector.Hidden();
            if (_selectChequer == targetComponent) return;
            Vector3 forceVector = new Vector3(position.x - _selectChequer.transform.position.x,
                _selectChequer.transform.position.y, position.z - _selectChequer.transform.position.z);
            _selectChequer.Toss(forceVector);
            _selectChequer = null;
            _isWaitStopped = true;
            StartCoroutine(CheckedEndTurn());
        }

        private IEnumerator CheckedEndTurn() {
            yield return new WaitForSeconds(0.5f);
            yield return new WaitWhile(() => !PlayerManager.Player.IsAllChequersStopped());
            PlayerManager.Player.AllChequersStopped();
            yield return new WaitWhile(() => !PlayerManager.Enemy.IsAllChequersStopped());
            PlayerManager.Enemy.AllChequersStopped();
            bool isOpponentTurns = IsTurn();
            if (isOpponentTurns) {
                IsCurrentWhiteSide = !IsCurrentWhiteSide;
            }

            _livingSpace.ClearChipsOutOfChequers();
            if (!CheckEndRound()) {
                BroadcastStateTurns(isOpponentTurns);
            }

            _isWaitStopped = false;
            MakeStep();
        }

        private void MakeStep() {
            if (PlayerManager.Enemy.IsWhiteColorChequer == IsCurrentWhiteSide) {
                _AIController.MakeStep();
            }
        }

        private void BroadcastStateTurns(bool isOpponentTurns) {
            if (isOpponentTurns) {
                Messenger<Player>.Broadcast(OPPONENT_TURNS,
                    IsCurrentWhiteSide == PlayerManager.Player.IsWhiteColorChequer
                        ? PlayerManager.Player
                        : PlayerManager.Enemy);
            }
            else {
                Messenger.Broadcast(ANOTHER_ATTEMPT);
            }
        }

        private bool CheckEndRound() {
            bool isPlayerLive = PlayerManager.Player.IsLive();
            bool isEnemyLive = PlayerManager.Enemy.IsLive();
            bool isEndRound = true;
            if (!isPlayerLive && !isEnemyLive) {
                NeutralResult();
            }
            else if (!isPlayerLive) {
                WinPlayer(PlayerManager.Enemy, PlayerManager.Player);
            }
            else if (!isEnemyLive) {
                WinPlayer(PlayerManager.Player, PlayerManager.Enemy);
            }
            else {
                isEndRound = false;
            }

            return isEndRound;
        }

        private void NeutralResult() {
            BroadcastWinPlayerRoundAndReset(null);
        }

        private void ResetGame(Player playerWin) {
            PlayerManager.Player.ResetStateChequers();
            PlayerManager.Enemy.ResetStateChequers();            
            IsCurrentWhiteSide = playerWin.IsWhiteColorChequer;
            StartGame();
        }

        private void BroadcastWinPlayerRoundAndReset(Player playerWin) {
            ResetGame(playerWin);
            Messenger<Player>.Broadcast(WIN_PLAYER_ROUND, playerWin);
        }

        private void WinPlayer(Player playerWin, Player playerLose) {
            playerWin.IncrementStepInBoard();
            if (!(playerWin.StepInBoard + playerLose.StepInBoard < Board.COUNT_CELL_IN_ROW - 1)) {
                playerLose.DecrementStepInBoard();
                if (playerLose.StepInBoard < 0) {
                    GameWin(playerWin);
                }
                else {
                    BroadcastWinPlayerRoundAndReset(playerWin);
                }
            }
            else {
                BroadcastWinPlayerRoundAndReset(playerWin);
            }
        }

        private void GameWin(Player playerWin) {
            Messenger<Player>.Broadcast(WIN_PLAYER_GAME, playerWin);
        }

        private bool IsTurn() {
            bool isChangeOfSide = _livingSpace.ChipsOutOfChequers.Count == 0;
            foreach (Chequer chequer in _livingSpace.ChipsOutOfChequers) {
                if (chequer.IsWhiteColor == PlayerManager.Player.IsWhiteColorChequer) {
                    PlayerManager.Player.ChequerToDeadLetter(chequer);
                }
                else {
                    PlayerManager.Enemy.ChequerToDeadLetter(chequer);
                }


                isChangeOfSide = isChangeOfSide || (IsCurrentWhiteSide == chequer.IsWhiteColor);
            }

            return isChangeOfSide;
        }
    }
}