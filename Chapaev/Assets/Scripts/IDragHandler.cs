using UnityEngine;

namespace Battle{
    public interface IDragHandler<T, S>{       
        
        void OnBeginDrag(T selectComponent, Vector3 position);

        void OnDrag(T selectComponent, Vector3 position);

        void OnEndDrag(T selectComponent, Vector3 position, T targetComponent);    
    }
}