namespace Battle.AI{
    public interface IAI{
        void Init(Player bot, Player enemy, IDragHandler<Chequer, Chequer> handler);
        void MakeStep();
        void Stopped();
    }
}