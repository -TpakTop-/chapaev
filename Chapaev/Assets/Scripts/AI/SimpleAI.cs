using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Battle.AI{
    public class SimpleAI : MonoBehaviour, IAI{
        /// <summary>
        /// Максимальное количество фишект из которых bot пытается атаковать
        /// </summary>
        [SerializeField] private int _maxOutPoint = 3;

        /// <summary>
        /// Минимальная длина ветора направления
        /// </summary>
        [SerializeField] private float _minLengthVector = 4;
        /// <summary>
        /// Максимальная длина ветора направления
        /// </summary>
        [SerializeField] private float _maxLengthVector = 7;
        private Player _bot;
        private Player _enemy;
        private IDragHandler<Chequer, Chequer> _battleHeader;
        private IEnumerator _currentStep;
        
        public void Init(Player bot, Player enemy, IDragHandler<Chequer, Chequer> handler) {
            _bot = bot;
            _enemy = enemy;
            _battleHeader = handler;
        }

        /// <summary>
        /// Сделать шаг
        /// </summary>
        public void MakeStep() {
            _currentStep = Step();
            StartCoroutine(_currentStep);
        }

        /// <summary>
        /// Остановка AI
        /// </summary>
        public void Stopped() {
            if (_currentStep != null) {                
                StopCoroutine(_currentStep);    
            }            
        }

        /// <summary>
        /// Шаг AI
        /// </summary>
        /// <returns></returns>
        private IEnumerator Step() {
            if (_enemy.Chequers.Count == 0) yield break;
            yield return new WaitForSeconds(1.5f);
            int countChequersForStep = _bot.Chequers.Count; 
            int countOutPoint = Random.Range(1, Mathf.Min(countChequersForStep, _maxOutPoint));
            for (int i = 0; i < countOutPoint; i++) {
                Chequer botChequer = GetRandomChequer(_bot.Chequers);                
                yield return Aim(botChequer, i == countOutPoint - 1);
            }
        }

        private Chequer GetRandomChequer(List<Chequer> chequers) {
            return chequers[Random.Range(0, chequers.Count - 1)];
        }

        /// <summary>
        /// Нацелеваие бота
        /// </summary>
        /// <param name="botChequer">шашка выбранная ботом для хода</param>
        /// <param name="finalDecision">финальное решение (true-значит бот будет бросать, иначе нет)</param>
        /// <returns></returns>
        private IEnumerator Aim(Chequer botChequer, bool finalDecision) {
            _battleHeader.OnBeginDrag(botChequer, botChequer.transform.position);            
            Chequer enemyChequer = GetRandomChequer(_enemy.Chequers);
            Vector3 targetPosition = GetTargetPosition(botChequer, enemyChequer);
            // Производиься наводка на указанную точку
            yield return Drag(botChequer, botChequer.transform.position, targetPosition);
            // Здесь мы получаем количество смен целей
            int countOutPoint = Random.Range(1, Mathf.Min(_enemy.Chequers.Count, _maxOutPoint));
            for (int i = 0; i < countOutPoint; i++) { 
                Chequer newEnemyChequer = GetRandomChequer(_enemy.Chequers);                
                yield return Drag(botChequer, enemyChequer.transform.position, newEnemyChequer.transform.position);
                yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
                enemyChequer = newEnemyChequer;
            }                        
            _battleHeader.OnEndDrag(botChequer, enemyChequer.transform.position, finalDecision ? null : botChequer);
        }

        /// <summary>
        /// Проверя условия по длине получаемого вектора между двумя шашками
        /// </summary>
        /// <param name="chequer">шашка которая бьет</param>
        /// <param name="targetChequer">шашка которую бьют</param>
        /// <returns></returns>
        private Vector3 GetTargetPosition(Chequer chequer, Chequer targetChequer) {            
            Vector3 vector = targetChequer.transform.position - chequer.transform.position;            
            if (vector.magnitude < _minLengthVector) {
                vector = chequer.transform.position + vector.normalized * _minLengthVector;
            }
            else if (vector.magnitude > _minLengthVector) {
                vector = chequer.transform.position + vector.normalized * _maxLengthVector;
            }

            return vector;
        }

        /// <summary>
        /// Плавное наведение шашки с startPosition на endPosition
        /// </summary>
        /// <param name="botChequer">шашка</param>
        /// <param name="startPosition">первоначальная точка на которую нацелена шашка</param>
        /// <param name="endPosition">конечная точка на которую будет нацелена шашка</param>
        /// <returns></returns>
        private IEnumerator Drag(Chequer botChequer, Vector3 startPosition, Vector3 endPosition) {
            float partJourney = 0.1f;
            Vector3 targetPosition = startPosition;
            while (!targetPosition.Equals (endPosition)) {
                targetPosition = Vector3.Lerp (startPosition, endPosition, partJourney);
                _battleHeader.OnDrag(botChequer, targetPosition);
                partJourney += 0.2f;
                yield return new WaitForSeconds(Random.Range(0.05f, 0.15f));
            }
        }
        
        
    }
}