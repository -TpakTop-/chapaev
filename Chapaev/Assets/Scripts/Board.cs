using System.Collections.Generic;
using UnityEngine;

namespace Battle{
    public class Board : MonoBehaviour{        
        public const int COUNT_CELL_IN_ROW = 8; 
        [SerializeField] private Vector3 _startPosition;
        [SerializeField] private float _sizeShift;
        [SerializeField] private GameObject _prefabChequer;
        [SerializeField] private Material _materialForWhiteChequer;
        [SerializeField] private Material _materialForBlackChequer;      
        public List<Chequer> WhiteChequer { get; private set; }
        public List<Chequer> BlackChequer { get; private set; }


        private void Awake() {
            Init();
        }

        private void Init() {
            WhiteChequer = GenerateChequersByMaterial(_materialForWhiteChequer);            
            SetWhiteChequers(WhiteChequer, 0);
            BlackChequer = GenerateChequersByMaterial(_materialForBlackChequer);
            SetBlackChequers(BlackChequer, 0);
        }    
        
        private List<Chequer> GenerateChequersByMaterial(Material material) {
            _prefabChequer.GetComponent<MeshRenderer>().material = material;
            List<Chequer> chequers = new List<Chequer>();
            for (int i = 0; i < COUNT_CELL_IN_ROW; i++) {
                Chequer chequer = Instantiate(_prefabChequer, transform).GetComponent<Chequer>();
                chequer.IsWhiteColor = _materialForWhiteChequer == material;
                chequers.Add(chequer);
            }

            return chequers;
        }
        
        public void SetChequers(Player player) {
            if (player.IsWhiteColorChequer) {
                SetWhiteChequers(player.Chequers, player.StepInBoard);
            }
            else {
                SetBlackChequers(player.Chequers, player.StepInBoard);
            }
        }
        
        private void SetWhiteChequers(List<Chequer> chequers, int step) {       
            Vector3 startPosition = _startPosition + step *_sizeShift * Vector3.forward;
            SetChequersByStartPosition(startPosition, chequers);
        }
        
        private void SetBlackChequers(List<Chequer> chequers, int step) {
            Vector3 startPosition = _startPosition + (COUNT_CELL_IN_ROW - 1 - step) *_sizeShift * Vector3.forward;
            SetChequersByStartPosition(startPosition, chequers);
        }

        private void SetChequersByStartPosition(Vector3 startPosition, List<Chequer> chequers) {
            Vector3 shiftVector = Vector3.right * _sizeShift;
            foreach (var chequer in chequers) {
                chequer.IsLive = true;
                chequer.Stopped();
                chequer.transform.localPosition = startPosition;
                startPosition += shiftVector;
            }
        }

        
    }
}