using UnityEngine;

namespace Battle{
    public class ScannerInput : MonoBehaviour{
        private const int LEFT_BUTTON_CODE = 0;

        [SerializeField]
        private BattleManager _handler;
        [SerializeField]
        private Camera _cameraMain;
        private bool _clickDown;
        private bool _clickUp;
        private float _distance;
        private Chequer _selectChequer;
        

        private void Awake() {
            _distance = _cameraMain.transform.position.y * 2;
        }

        private void Update() {
            _clickUp = Input.GetMouseButtonUp(LEFT_BUTTON_CODE);
            if (_clickUp) {
                _clickDown = false;                
            }

            RaycastHit hit;            
            if (Physics.Raycast(_cameraMain.ScreenPointToRay(Input.mousePosition), out hit, _distance)) {                
                Transform hitTransform = hit.transform;

                if (Input.GetMouseButtonDown(LEFT_BUTTON_CODE)) {
                    _clickDown = true;
                    _selectChequer = hitTransform.GetComponent<Chequer>();
                    if (_selectChequer) {
                        _handler.OnBeginDrag(_selectChequer, hit.point);    
                    }                    
                }
                else if (_clickDown && _selectChequer) {
                    _handler.OnDrag(_selectChequer, hit.point);
                }
                else if (_clickUp && _selectChequer) {
                    _handler.OnEndDrag(_selectChequer, hit.point, hitTransform.GetComponent<Chequer>());
                    _selectChequer = null;
                }              
            }           
        }
    }
}