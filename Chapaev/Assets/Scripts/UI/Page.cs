using UnityEngine;

namespace UI{
    public abstract class Page : MonoBehaviour{
        public void Show() {
            gameObject.SetActive(true);
        }

        public void Hidden() {
            gameObject.SetActive(false);
        }
    }
}