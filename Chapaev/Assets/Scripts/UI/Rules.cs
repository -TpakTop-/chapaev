using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class Rules : Page{
        public const string HAVE_READ_RULE = "HAVE_READ_RULE";
        
        [SerializeField] private Button _haveReadRulesButton;

        private void Awake() {
            _haveReadRulesButton.onClick.AddListener(() => {
                GameSound.Instance.PlayButton();
                Messenger.Broadcast(HAVE_READ_RULE);
            });
        }     
        
    }
}