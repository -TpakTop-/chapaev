using Battle;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class UIPlaying : Page{
        public const string SHOW_UI_MAIN = "SHOW_UI_MAIN";
        public const string REPEAT_GAME = "REPEAT_GAME";
        [SerializeField] private Button _menu;
        [SerializeField] private OpponentTurns _opponentTurns;
        [SerializeField] private UIFinishGame _uiFinishGame;

        private void Awake() {
            _menu.onClick.AddListener(BroadcastShowMain);
            _opponentTurns.Hidden();
            _uiFinishGame.Hidden();
            Messenger.AddListener(UIFinishGame.FINISH_GAME_BACK, BroadcastShowMain);
            Messenger.AddListener(UIFinishGame.FINISH_GAME_REPEAT, RepeatGame);
        }

        private void BroadcastShowMain() {
            GameSound.Instance.PlayButton();
            Messenger.Broadcast(SHOW_UI_MAIN);
        }

        private void RepeatGame() {
            Messenger.Broadcast(REPEAT_GAME);
        }

        public void ShowOpponentTurns(Player player) {
            _opponentTurns.Show(player);
        }

        public void ShowUIFinishGame(Player playerWin) {
            _uiFinishGame.Show(playerWin);
        }

        private void OnDestroy() {
            Messenger.RemoveListener(UIFinishGame.FINISH_GAME_BACK, BroadcastShowMain);
            Messenger.RemoveListener(UIFinishGame.FINISH_GAME_REPEAT, RepeatGame);
        }
    }
}