using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class ChoiceColor : Page{
        public const string PLAYER_CHOICE_COLOR = "PLAYER_CHOICE_COLOR";
        
        [SerializeField] private Button _whiteColorButton;
        [SerializeField] private Button _blackColorButton;

        private void Awake() {
            _whiteColorButton.onClick.AddListener(() => BroadcastMessage(PLAYER_CHOICE_COLOR, true));
            _blackColorButton.onClick.AddListener(() => BroadcastMessage(PLAYER_CHOICE_COLOR, false));
        }
        
        private void BroadcastMessage(string message, bool isWhiteColor) {
            GameSound.Instance.PlayButton();            
            Messenger<bool>.Broadcast(message, isWhiteColor);
        }
    }
}