﻿using Battle;
using Battle.AI;
using UnityEngine;

namespace UI{
    public class UIManager : MonoBehaviour{
        private UIMain _uiMain;
        private UIPlaying _uiPlaying;
        private ScannerInput _scannerInput;
        private BattleManager _battleManager;
        private Notebook _notebook;


        private void Awake() {
            _uiMain = GetComponentInChildren<UIMain>();
            _uiPlaying = GetComponentInChildren<UIPlaying>();
            _battleManager = FindObjectOfType<BattleManager>();
            _scannerInput = FindObjectOfType<ScannerInput>();
            _notebook = FindObjectOfType<Notebook>();

            AddListeners();
        }

        private void AddListeners() {
            Messenger.AddListener(Menu.EXIT, ExitGame);
            Messenger<bool>.AddListener(ChoiceColor.PLAYER_CHOICE_COLOR, StartGame);
            Messenger.AddListener(UIPlaying.SHOW_UI_MAIN, ShowUIMain);
            Messenger<Player>.AddListener(BattleManager.OPPONENT_TURNS, OpponentsTurns);
            Messenger<Player>.AddListener(BattleManager.WIN_PLAYER_GAME, WinPlayerGame);
            Messenger<Player>.AddListener(BattleManager.WIN_PLAYER_ROUND, WinPlayerRound);
            Messenger.AddListener(BattleManager.ANOTHER_ATTEMPT, AnotherAttempt);
            Messenger.AddListener(UIPlaying.REPEAT_GAME, RepeatGame);
        }

        private void RemoveListeners() {
            Messenger.RemoveListener(Menu.EXIT, ExitGame);
            Messenger<bool>.RemoveListener(ChoiceColor.PLAYER_CHOICE_COLOR, StartGame);
            Messenger.RemoveListener(UIPlaying.SHOW_UI_MAIN, ShowUIMain);
            Messenger<Player>.RemoveListener(BattleManager.OPPONENT_TURNS, OpponentsTurns);
            Messenger<Player>.RemoveListener(BattleManager.WIN_PLAYER_GAME, WinPlayerGame);
            Messenger<Player>.RemoveListener(BattleManager.WIN_PLAYER_ROUND, WinPlayerRound);
            Messenger.RemoveListener(BattleManager.ANOTHER_ATTEMPT, AnotherAttempt);
            Messenger.RemoveListener(UIPlaying.REPEAT_GAME, RepeatGame);
        }

        private void RepeatGame() {
            StartGame(_battleManager.PlayerManager.Player.IsWhiteColorChequer);
        }

        private void WinPlayerRound(Player playerWin) {
            _notebook.Init(_battleManager);
            SetEnabledScannerInput();
        }

        private void WinPlayerGame(Player playerWin) {
            _scannerInput.enabled = false;
            _uiPlaying.ShowUIFinishGame(playerWin);
            GameMusic.Instance.Stop();
            if (playerWin == _battleManager.PlayerManager.Player) {
                GameSound.Instance.PlayWin();
            }
            else {
                GameSound.Instance.PlayLoser();
            }
        }

        private void StartGame(bool isPlayerChoiceWhiteChequer) {
            Time.timeScale = 1f;
            _uiMain.Hidden();
            _uiPlaying.Show();
            _battleManager.InitGame(isPlayerChoiceWhiteChequer, FindObjectOfType<SimpleAI>());
            _battleManager.StartGame();
            SetEnabledScannerInput();
            _notebook.Init(_battleManager);
            GameMusic.Instance.Play();
        }

        private void OpponentsTurns(Player player) {
            _uiPlaying.ShowOpponentTurns(player);
            _notebook.UpdateInformationPlayers(_battleManager);
            SetEnabledScannerInput();
        }

        public void SetEnabledScannerInput() {
            _scannerInput.enabled = _battleManager.PlayerManager.Player.IsWhiteColorChequer ==
                                    _battleManager.IsCurrentWhiteSide;
        }

        private void AnotherAttempt() {
            _notebook.UpdateInformationPlayers(_battleManager);
        }

        private void Start() {
            ShowUIMain();
        }

        private void ExitGame() {
            Application.Quit();
        }

        private void ShowUIMain() {
            Time.timeScale = 0f;
            GameMusic.Instance.Stop();
            _uiPlaying.Hidden();
            _uiMain.Show();
            _scannerInput.enabled = false;
        }

        private void OnDestroy() {
            RemoveListeners();
        }
    }
}