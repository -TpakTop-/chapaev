﻿using Battle;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class InfoPlayer : MonoBehaviour{
        private const string FORMAT_DESCRIPTION = "Выйгранно партий: {0}";
        private Text _title;
        private Text _description;
        private ChequerImages _chequerImages;
        private Player _player;
        private string _formatDescriptionText;

        private void Awake() {
            Text[] texts = GetComponentsInChildren<Text>();
            _title = texts[0];
            _description = texts[1];
            _chequerImages = GetComponentInChildren<ChequerImages>();
        }

        public void Init(Player player) {
            _player = player;
            _title.text = player.Name;
            _chequerImages.Clear();
            _formatDescriptionText =
                string.Format("{0}\n{1}", player.IsWhiteColorChequer ? "Белые" : "Черные", FORMAT_DESCRIPTION);
            _chequerImages.Init(player.IsWhiteColorChequer ? Color.black : Color.white);
            ShowDescription();
        }

        private void ShowDescription() {
            _description.text = string.Format(_formatDescriptionText, _player.CountWinRound);
        }

        public void UpdateInfo(Player enemy) {
            ShowDescription();
            _chequerImages.AddItemsCount(enemy.CountDead() - _chequerImages.GetCountItems());
        }
    }
}