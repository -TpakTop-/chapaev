using Battle;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class Notebook : MonoBehaviour{
        [SerializeField] private InfoPlayer _infoPlayer;
        [SerializeField] private InfoPlayer _infoEnemy;
        [SerializeField] private Text _currentPlayerTurnText;      
        

        public void Init(BattleManager battleManager) {
            _infoPlayer.Init(battleManager.PlayerManager.Player);            
            _infoEnemy.Init(battleManager.PlayerManager.Enemy);     
            ShowCurrentPlayerTurnText(battleManager.IsCurrentWhiteSide);
        }
        
        public void UpdateInformationPlayers(BattleManager battleManager) {
            _infoPlayer.UpdateInfo(battleManager.PlayerManager.Enemy);
            _infoEnemy.UpdateInfo(battleManager.PlayerManager.Player);
            ShowCurrentPlayerTurnText(battleManager.IsCurrentWhiteSide);
        }

        private void ShowCurrentPlayerTurnText(bool isCurrentWhiteSide) {
            _currentPlayerTurnText.text = string.Format("{0} ходят", isCurrentWhiteSide ? "Белые" : "Черные");
        }
    }
}