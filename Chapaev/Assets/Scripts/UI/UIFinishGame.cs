using Battle;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class UIFinishGame : Page{
        public const string FINISH_GAME_BACK = "FINISH_GAME_BACK";
        public const string FINISH_GAME_REPEAT = "FINISH_GAME_REPEAT";
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _repeatButton;
        [SerializeField] private Text _winner;

        private void Awake() {
            _backButton.onClick.AddListener(() => CloseAndBroadcastMessage(FINISH_GAME_BACK));
            _repeatButton.onClick.AddListener(() => CloseAndBroadcastMessage(FINISH_GAME_REPEAT));
        }

        private void CloseAndBroadcastMessage(string message) {
            GameSound.Instance.PlayButton();
            Hidden();
            Messenger.Broadcast(message);
        }

        public void Show(Player playerWin) {
            Show();
            _winner.text = string.Format("Победил {0}\nХотите сыграть еще?", playerWin.Name);
        }
        
        
    }
}