namespace UI{
    public class UIMain : Page{
        private Menu _menu;
        private Rules _rules;
        private ChoiceColor _choiceColor;
        private Page[] _pages;

        private void Awake() {            
            Messenger.AddListener(Menu.RULES_GAME, ShowRules);
            Messenger.AddListener(Menu.NEW_GAME, ChoiceColor);
            Messenger.AddListener(Rules.HAVE_READ_RULE, ShowMenu);
            InitPages();
        }

        private void InitPages() {
            _menu = FindObjectOfType<Menu>();
            _rules = FindObjectOfType<Rules>();
            _choiceColor = FindObjectOfType<ChoiceColor>();
            _pages = new Page[] {_menu, _rules, _choiceColor};
        }

        private void ChoiceColor() {
            ShowPage(_choiceColor);
        }

        private void ShowMenu() {
            ShowPage(_menu);            
        }

        private void ShowRules() {
            ShowPage(_rules);
        }

        private void ShowPage(Page pageForShow) {
            foreach (var page in _pages) {
                if (page == pageForShow) {
                    page.Show();
                }
                else {
                    page.Hidden();
                }
            }
        }

        private void OnEnable() {
            ShowMenu();            
        }
        
        private void OnDestroy() {
            Messenger.RemoveListener(Menu.RULES_GAME, ShowRules);
            Messenger.RemoveListener(Rules.HAVE_READ_RULE, ShowMenu);
        }
    }
}