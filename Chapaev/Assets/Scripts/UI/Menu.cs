using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class Menu : Page{
        public const string NEW_GAME = "NEW_GAME";
        public const string RULES_GAME = "RULES_GAME";
        public const string EXIT = "EXIT";
        
        [SerializeField] private Button _newGameButton;
        [SerializeField] private Button _rulesGameButton;
        [SerializeField] private Button _exitButton;

        private void Awake() {
            _newGameButton.onClick.AddListener(() => BroadcastMessage(NEW_GAME));
            _rulesGameButton.onClick.AddListener(() => BroadcastMessage(RULES_GAME));
            _exitButton.onClick.AddListener(() => BroadcastMessage(EXIT));
        } 
        
        private void BroadcastMessage(string message) {
            GameSound.Instance.PlayButton();            
            Messenger.Broadcast(message);
        }
    }
}