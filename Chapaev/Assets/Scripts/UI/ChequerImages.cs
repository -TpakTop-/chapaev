using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class ChequerImages : MonoBehaviour{
        [SerializeField] private GameObject _prefabChequer;
        private Color _color = Color.white;
        
        public void Init(Color color) {
            _color = color;
            Clear();
        }

        public void AddItemsCount(int count) {            
            for (int i = 0; i < count; i++) {
                Image image = Instantiate(_prefabChequer, transform).GetComponent<Image>();
                image.color = _color;
            }
        }

        public int GetCountItems() {
            return GetComponentsInChildren<Image>().Length;
        }

        public void Clear() {
            Image[] images = GetComponentsInChildren<Image>();
            foreach (Image image in images) {
                Destroy(image.gameObject);
            }
        }
    }
}