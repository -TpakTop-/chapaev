using System.Collections;
using Battle;
using UnityEngine;
using UnityEngine.UI;

namespace UI{
    public class OpponentTurns : Page{
        [SerializeField] private Text _info;
        [SerializeField] private float _waitTimeSecondForFlash = 1f;
        
        public void Show(Player player) {
            Show();
            StartCoroutine(Flash(player));
        }

        private IEnumerator Flash(Player player) {            
            _info.text = string.Format("Ходит {0}", player.Name);
            yield return new WaitForSeconds(_waitTimeSecondForFlash); 
            Hidden();
        }
    }
}