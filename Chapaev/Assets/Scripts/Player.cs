using System.Collections.Generic;
using UnityEngine;

namespace Battle{
    public class Player{
        public string Name { get; private set; }
        public bool IsWhiteColorChequer { get; private set; }
        public int StepInBoard { get; private set; }
        public int CountWinRound { get; private set; }
        public List<Chequer> Chequers { get; private set; }
        private List<Chequer> _deadLetter;


        public Player(string name, bool isWhiteColorChequer, List<Chequer> chequers) {
            IsWhiteColorChequer = isWhiteColorChequer;
            Chequers = new List<Chequer>(chequers);
            _deadLetter = new List<Chequer>();
            Name = name;
            StepInBoard = 0;
        }

        public void ChequerToDeadLetter(Chequer chequer) {
            if (Chequers.Remove(chequer)) {
                _deadLetter.Add(chequer);
            }
        }

        public bool IsAllChequersStopped() {
            foreach (Chequer chequer in Chequers) {                
                if (!chequer.IsStopped()) return false;
            }

            return true;
        }    

        public void AllChequersStopped() {
            StopListChequer(Chequers);            
        }

        private void StopListChequer(List<Chequer> chequers) {
            foreach (Chequer chequer in chequers) {
                chequer.Stopped();
            }
        }

        public bool IsLive() {
            return Chequers.Count > 0;
        }

        public void ResetStateChequers() {
            Chequers.AddRange(_deadLetter);
            _deadLetter.Clear();
        }

        public int IncrementStepInBoard() {
            CountWinRound++;
            return StepInBoard++;
        }

        public int DecrementStepInBoard() {
            return StepInBoard--;
        }

        public int CountDead() {
            return _deadLetter.Count;
        }
    }
}