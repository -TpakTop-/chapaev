using UnityEngine;

namespace Battle{
    [RequireComponent(typeof(Rigidbody))]
    public class Chequer : MonoBehaviour{
        public float Power = 100;
        private Rigidbody _rigidbody;
        public bool IsWhiteColor;        
        public bool IsLive;

        private void Awake() {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Toss(Vector3 forceVector) {            
            _rigidbody.AddForce(forceVector * Power);
        }

        public bool IsStopped() {
            return _rigidbody.velocity.magnitude < 0.2f;
        }

        public void Stopped() {
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;     
        }

        public float GetMass() {
            return _rigidbody.mass;
        }     
    }
}