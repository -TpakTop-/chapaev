﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class GameMusic : MonoBehaviour{    
    private AudioSource _audioSource;

    [Header("Audio clops")] [SerializeField]
    private AudioClip[] _sounds;

    private ResourceRequest _status;
    public bool IsPlay { get; private set; }

    public static GameMusic Instance { get; private set; }

    void Awake() {
        _audioSource = GetComponent<AudioSource>();        
        if (Instance == null) {
            // Экземпляр менеджера был найден
            Instance = this; // Задаем ссылку на экземпляр объекта
        }
        else if (Instance == this) {
            // Экземпляр объекта уже существует на сцене
            Destroy(gameObject); // Удаляем объект
        }

        // Теперь нам нужно указать, чтобы объект не уничтожался
        // при переходе на другую сцену игры
        DontDestroyOnLoad(gameObject);
    }

    void AddSoundInArray(ref AudioClip[] array, AudioClip audioClip) {
        int index = array.Length;
        Array.Resize(ref array, index + 1);
        array[index] = audioClip;
    }


    void FixedUpdate() {
        if (!IsPlay) {
            return;
        }

        if (!_audioSource.isPlaying) {
            Play(_sounds);            
        }
    }

    public void Play() {
        IsPlay = true;
    }
    
    private void Play(AudioClip[] clips) {
        IsPlay = true;
        _audioSource.Stop();
        if (clips.Length > 0) {
            _audioSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
        }
    }

    public void Stop() {
        IsPlay = false;
        _audioSource.Stop();
    }

    public void Delete() {
        _audioSource.Stop();
        Instance = null;
        Destroy(gameObject);
    }

    public bool GetMute() {
        return _audioSource.mute;
    }

    public void SetMute(bool mute) {
        _audioSource.mute = mute;
    }

    public void SetPause(bool isPause) {
        if (isPause) {
            _audioSource.Pause();
        }
        else {
            _audioSource.UnPause();
        }
    }  
}