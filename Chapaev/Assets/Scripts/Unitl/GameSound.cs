﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GameSound : MonoBehaviour{
    private AudioSource _audioSource;

    [Header("AudioClips events")]
    [SerializeField] private AudioClip _button;   
    [SerializeField] private AudioClip _loser;
    [SerializeField] private AudioClip _win;
    [SerializeField] private AudioClip _collision;   

    public static GameSound Instance { get; private set; }

    void Awake() {
        _audioSource = GetComponent<AudioSource>();
        if (Instance == null) {
            // Экземпляр менеджера был найден
            Instance = this; // Задаем ссылку на экземпляр объекта
        }
        else if (Instance == this) {
            // Экземпляр объекта уже существует на сцене
            Destroy(gameObject); // Удаляем объект
        }

        // Теперь нам нужно указать, чтобы объект не уничтожался
        // при переходе на другую сцену игры
        DontDestroyOnLoad(gameObject);
    }

    public void PlayButton() {
        _audioSource.PlayOneShot(_button);
    }   

    public void PlayLoser() {
        _audioSource.PlayOneShot(_loser);
    }

    public void PlayWin() {
        _audioSource.PlayOneShot(_win);
    }

    public void PlayCollision() {
        _audioSource.PlayOneShot(_collision);
    }
   
    public void Delete() {
        if (_audioSource != null) {
            _audioSource.Stop();
        }

        Instance = null;
        Destroy(gameObject);
    }

    public bool GetMute() {
        return _audioSource.mute;
    }

    public void SetMute(bool mute) {
        _audioSource.mute = mute;
    }
}